import { InAppBrowser,InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
    url = 'http://191.252.196.74:8080';
    constructor(private InAppBrowser: InAppBrowser){
    }

    openWebPage(url:string){      
      const browser = this.InAppBrowser.create(url, '_system');      
    }

    doRefresh(refresher){      
      const browser = this.InAppBrowser.create(this.url, 'http://191.252.196.74:8080');
      browser.on('load').toPromise()
      .then(()=>{
        refresher.complete()
      })
      .catch((err)=>{
        console.log('error',err)
      })
    }
}
